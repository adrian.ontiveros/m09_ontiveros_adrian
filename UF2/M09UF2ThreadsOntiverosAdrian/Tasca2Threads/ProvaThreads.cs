﻿using System;
using System.Threading;

namespace Tasca2Threads
{
    class ProvaThreads
    {
        public static void Main(string[] args)
        {
            Nevera nevera = new Nevera();

            // Creamos los threads para llenar la nevera y beber cervezas
            Thread anittaThread = new Thread(() => nevera.LlenarNevera("Anitta"));
            Thread badBunnyThread = new Thread(() => nevera.BeberCerveza("Bad Bunny"));
            Thread lilNasXThread = new Thread(() => nevera.BeberCerveza("Lil Nas X"));
            Thread manuelTurizoThread = new Thread(() => nevera.BeberCerveza("Manuel Turizo"));

            // Iniciamos y vamos ejecutando los threads
            anittaThread.Start();
            anittaThread.Join();

            badBunnyThread.Start();
            badBunnyThread.Join();

            lilNasXThread.Start();
            lilNasXThread.Join();

            manuelTurizoThread.Start();
            manuelTurizoThread.Join();
        }
    }
}
