﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasca2Threads
{
    public class Nevera
    {
        public const int MAX_CERVEZAS = 9;
        public List<int> Cervezas;

        public Nevera()
        {
            Cervezas = new List<int>();
        }

        public void LlenarNevera(string persona)
        {
            int cantidadCervezas = new Random().Next(1, 7); 
            int cervezasActuales = Cervezas.Count;

            // Si se supera el máximo, se ajusta la cantidad
            if (cervezasActuales + cantidadCervezas > MAX_CERVEZAS) cantidadCervezas = MAX_CERVEZAS - cervezasActuales; 

            for (int i = 0; i < cantidadCervezas; i++) Cervezas.Add(1);

            Console.WriteLine($"{persona} ha llenado la nevera con {cantidadCervezas} cervezas, olee");
        }

        public void BeberCerveza(string persona)
        {
            if (Cervezas.Count == 0)
            {
                Console.WriteLine("La nevera está vacía, borrachos");
                return;
            }

            int cantidadCervezas = new Random().Next(1, 7);
            int cervezasActuales = Cervezas.Count;

            // Si se intenta beber más cervezas de las que hay, se reduce a la cantidad que haya
            if (cantidadCervezas > cervezasActuales) cantidadCervezas = cervezasActuales;

            for (int i = 0; i < cantidadCervezas; i++) Cervezas.RemoveAt(0);

            Console.WriteLine($"{persona} se ha bebido {cantidadCervezas} cervezas... vaya borrachín");
        }
    }
}
