﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P2FilosofosOntiverosAdrian
{
    public class Palillo
    {
        public int id;

        public Palillo(int id)
        {
            this.id = id;
        }

        public int Id
        {
            get { return id; }
        }
    }
}
