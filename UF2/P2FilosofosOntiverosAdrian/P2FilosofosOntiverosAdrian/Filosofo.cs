﻿using System;
using System.Threading;

namespace P2FilosofosOntiverosAdrian
{
    class Filosofo
    {
        public int Id;
        public Palillo Izquierdo, Derecho;
        public bool Comido = false;

        public Filosofo(int id, Palillo izquierdo, Palillo derecho)
        {
            Id = id;
            Izquierdo = izquierdo;
            Derecho = derecho;
        }

        public void Comer()
        {
            Console.WriteLine($"Filósofo {Id} intentando tomar palillos {Izquierdo.Id} y {Derecho.Id}, esperando...");
            lock (Izquierdo)
            {
                Console.WriteLine($"Filósofo {Id} tiene el palillo izquierdo {Izquierdo.Id} ");
                lock (Derecho)
                {
                    Console.WriteLine($"Filósofo {Id} tiene ambos palillos {Izquierdo.Id} y {Derecho.Id}");
                    Console.WriteLine($"Filósofo {Id} está comiendo");
                    Thread.Sleep(2000);
                }
                Console.WriteLine($"Filósofo {Id} soltó el palillo derecho {Derecho.Id}");
            }
            Console.WriteLine($"Filósofo {Id} soltó el palillo izquierdo {Izquierdo.Id}");
            Comido = !Comido;
        }

        public void Pensar()
        {
            Console.WriteLine($"Filósofo {Id} está pensando");
            Thread.Sleep(1000);
        }

        public void Ejecutar()
        {
            while (!Comido)
            {
                Pensar();
                Comer();
            }
        }
    }
}
