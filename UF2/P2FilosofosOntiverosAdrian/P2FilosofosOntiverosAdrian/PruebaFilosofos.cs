﻿using System;
using System.Threading;

namespace P2FilosofosOntiverosAdrian
{
    class PruebaFilosofos
    {
        public static void Main(string[] args)
        {
            Palillo[] palillos = new Palillo[5];
            for (int i = 1; i < 6; i++)
            {
                palillos[i-1] = new Palillo(i);
            }

            Filosofo[] filosofos = new Filosofo[5];
            for (int i = 1; i < 6; i++)
            {
                int p2;

                if (i == 5) p2 = 0;
                else p2 = i;

                filosofos[i-1] = new Filosofo(i, palillos[i-1], palillos[p2]);
            }

            Thread[] hilos = new Thread[5];
            for (int i = 0; i < 5; i++)
            {
                hilos[i] = new Thread(new ThreadStart(filosofos[i].Ejecutar));
                hilos[i].Start();
            }

            for (int i = 0; i < 5; i++)
            {
                hilos[i].Join();
            }

            Console.WriteLine("Todos los filósofos han terminado de comer");
        }
    }
}
