﻿using System;
using System.Threading;

namespace ArreCaballito
{
    public class Caballo
    {
        public string Nombre;
        public int Posicion;
        public int Velocidad;
        public Thread Hilo;

        public Caballo(string nombre, int velocidad)
        {
            Nombre = nombre;
            Posicion = 0;
            Velocidad = velocidad;
            Hilo = new Thread(Avanzar);
        }

        public void Avanzar()
        {
            while (Posicion < 100)
            {
                Posicion += Velocidad;
                Thread.Sleep(1000);
            }
        }
    }
}
