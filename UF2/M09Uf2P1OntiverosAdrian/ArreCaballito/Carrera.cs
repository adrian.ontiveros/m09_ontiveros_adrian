﻿using System;
using System.Threading;

namespace ArreCaballito
{
    class Carrera
    {
        public Caballo[] Caballos;

        public Carrera()
        {
            Caballos = new Caballo[6];
            Caballos[0] = new Caballo("Caballo 1", new Random().Next(1, 5));
            Caballos[1] = new Caballo("Caballo 2", new Random().Next(1, 5));
            Caballos[2] = new Caballo("Caballo 3", new Random().Next(1, 5));
            Caballos[3] = new Caballo("Caballo 4", new Random().Next(1, 5));
            Caballos[4] = new Caballo("Caballo 5", new Random().Next(1, 5));
            Caballos[5] = new Caballo("Caballo 6", new Random().Next(1, 5));
        }

        public void IniciarCarrera()
        {
            Console.WriteLine("Presiona la tecla 'C' para iniciar la carrera");
            while (Console.ReadKey().Key != ConsoleKey.C) { }

            for (int i = 0; i < Caballos.Length; i++) Caballos[i].Hilo.Start();

            while (true)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Carrera de Caballos  -  Hagan sus apuestas");
                Console.ResetColor();
                Console.WriteLine("------------------------------------------\n");
                for (int i = 0; i < Caballos.Length; i++)
                {
                    Console.Write($"\n{Caballos[i].Nombre}: ");
                    for (int a = 0; a < Caballos[i].Posicion; a++) Console.Write("-");
                    Console.Write($"<o> { Caballos[i].Posicion}%\n");
                    Caballos[i].Velocidad = new Random().Next(1, 5);

                    if (IsGanador(Caballos[i])) return;
                }
                Thread.Sleep(1000);
            }
        }

        public bool IsGanador(Caballo caballo)
        {
            if (caballo.Posicion >= 100)
            {
                Console.Clear();
                for (int i = 0; i < Caballos.Length; i++)
                {
                    Console.Write($"\n{Caballos[i].Nombre}: ");
                    for (int a = 0; a < Caballos[i].Posicion; a++) Console.Write("-");
                    Console.Write($"{ Caballos[i].Posicion}%\n");
                }
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine($"\n\n¡{caballo.Nombre} ha ganado la carrera!\n");
                Console.ResetColor();
                return true;
            }
            else return false;
        }
    }
}
